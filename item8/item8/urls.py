from django.conf.urls import url
from main import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^get_time/$', views.get_time, name='get_time'),
]
