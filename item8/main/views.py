from django.conf import settings
from django.http import JsonResponse
from django.shortcuts import render
import datetime
import pytz


def index(request):
    return render(request, 'index.html')


def get_time(request):
    response = {}
    if request.POST.get('tz'):

        client_time = datetime.datetime.now(pytz.timezone(request.POST.get('tz')))
        server_time = datetime.datetime.now(pytz.timezone(settings.TIME_ZONE))
        diff = (server_time.utcoffset() - client_time.utcoffset())

        response['client_time'] = client_time.strftime("%A %d. %B %Y. %I:%M:%S. %Z")
        response['server_time'] = server_time.strftime("%A %d. %B %Y. %I:%M:%S. %Z")
        response['difference'] = str(diff.seconds//3600) + 'h'

    return JsonResponse(response)
