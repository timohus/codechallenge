dirty_list = [1, 2, 3, [[], 4, 5, [], [6, 7, []]], [], 8, [[[]], [], 9, 10]]


def clean_up(input_list):
    return list(filter(None, map(lambda x: clean_up(x) if isinstance(x, list) else x, input_list)))

print(clean_up(dirty_list))
