numbers = [x**2 if x % 2 == 0 else x for x in range(1, 11)]

print(numbers)
