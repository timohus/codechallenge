from django.db import models


class Book(models.Model):
    title = models.CharField(max_length=255)
    author = models.CharField(max_length=255)
    isbn = models.CharField(max_length=255)

    class Meta:
        db_table = 'book'

    def __str__(self):
        return self.title


class List(models.Model):
    name = models.CharField(max_length=255)
    books = models.ManyToManyField(Book, through='BookList')

    class Meta:
        db_table = 'list'

    def __str__(self):
        return self.name


class BookList(models.Model):
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    list = models.ForeignKey(List, on_delete=models.CASCADE)
    order = models.IntegerField(default=1)
    comment = models.TextField()

    class Meta:
        db_table = 'book_list'
        ordering = ['order']

    def __str__(self):
        return self.book.title + ' in ' + self.list.name

