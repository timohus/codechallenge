from django.contrib import admin
from main.models import Book, List, BookList


class BookListInline(admin.TabularInline):
    model = BookList
    extra = 1


class BookAdmin(admin.ModelAdmin):
    inlines = (BookListInline,)


class ListAdmin(admin.ModelAdmin):
    inlines = (BookListInline,)


admin.site.register(Book, BookAdmin)
admin.site.register(List, ListAdmin)