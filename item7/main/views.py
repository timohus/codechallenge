from django.shortcuts import render

from main.models import List


def index(request):
    lists = List.objects.all()
    return render(request, 'index.html', {'lists': lists})
