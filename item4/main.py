import numpy as np
import pandas as pd

ROWS = 10  # number of rows to be generated in dataframe

data_frame = pd.DataFrame({
    'A': np.random.choice(['John', 'Mike', 'Sam'], 10),
    'B': np.random.normal(0, 10, ROWS),
    'C': np.random.uniform(0, 10, ROWS),
    'D': np.random.normal(100, 15, ROWS)
}, dtype='int64')


def remove_duplicates(df, column):
    return df.drop_duplicates(subset=column, keep='first', inplace=False)


# Print out original dataframe with duplicates
print('\n Dirty:')
print(data_frame)


# Print out cleaned up dataframe
print('\n Cleaned:')
print(remove_duplicates(data_frame, 'A'))
