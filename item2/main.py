import numpy as np
import pandas as pd

ROWS = 10   # number of rows to be generated in dataframe

data = pd.DataFrame({
    '1.String': [''.join(symbol) for symbol in np.random.choice(list(map(chr, range(97, 123))), [ROWS, 8])],
    '2.Normal DD': np.random.normal(0, 10, ROWS),
    '3.Uniform DD': np.random.uniform(0, 10, ROWS),
    '4.IQ': np.random.normal(100, 15, ROWS)
}, dtype='int64')

print(data)
