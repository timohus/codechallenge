from django.shortcuts import render


def index(request):
    message = None
    return render(request, 'index.html', {'message': message})


def about(request):
    return render(request, 'about.html')


def contact(request):
    return render(request, 'contact.html')


# The view for non-existent page requests
def no_such_page(request):
    return render(request, 'no_such_page.html')
