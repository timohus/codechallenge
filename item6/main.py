variation_prices = {
    'orange': 12.0,
    'green': 16.0,
    'blue': 17.0
}

default = 10.0


def get_variation_price(variation):
    return variation_prices[variation]


def fail_with(func):
    def get_price(city):
        try:
            return func(city)
        except:
            return default
    return get_price

search_price = fail_with(get_variation_price)


print(search_price('green'))
print(search_price('white'))  # white does not exist
